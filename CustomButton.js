import React, { Component } from 'react'
import {BackHandler, TextInput, Dimensions, StyleSheet, Text, View, Image, Alert, Platform, TouchableOpacity, AsyncStorage, ScrollView} from 'react-native'

import { toDp } from './percentageToDP'

let { width, height } = Dimensions.get('window')

class CustomButton extends Component {

  render() {
    const {
        text,
        onPress,
        type, //gradient / tranparent,
        colors
    } = this.props
    return (
      <View style={styles.container}>
        <View style={styles.button}>
          <TouchableOpacity
            activeOpacity={1}
            style={[styles.touch, {borderWidth: colors ? 0 : type === 'gradient' ? 0 : toDp(0.47), borderColor: type === 'gradient' ? 'tranparent' : '#353A50'}]}
            onPress={onPress}
          >
            {/*<Text style={[styles.text, {color: colors ? '#111111' : type === 'gradient' ? 'white' : '#111111'}]}>{text}</Text>*/}
            <Text style={[styles.text, {color: type === 'gradient' ? '#111111' : '#F9F9F9'}]}>{text.toUpperCase()}</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: toDp(52),
  },
  button: {
    borderRadius: toDp(4),
    backgroundColor: '#EF9038'
  },
  touch: {
    width: '100%',
    height: toDp(52),
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    color: '#111111',
    fontSize: toDp(16),
    letterSpacing: toDp(0.75),
    fontWeight: 'bold'
  },
  lineGradient: {
    width: '100%',
    height: toDp(26),
    resizeMode: 'contain',
    bottom: toDp(4),
    zIndex: Platform.OS === 'ios' ? -1 : 0
  }

})


export default CustomButton
