/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useState } from 'react'
import type {Node} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  ActivityIndicator
} from 'react-native';

import CustomTextInput from './CustomTextInput'
import CustomButton from './CustomButton'
import { toDp } from './percentageToDP'

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

const App: () => Node = () => {
  const isDarkMode = useColorScheme() === 'dark';
  const [state, setState] = useState({
    city: '',
    errorCity: '',
    response: null,
    loading: false
  })

  const backgroundStyle = {
    backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
    //padding: toDp(24)
  };

  const lihat = () => {
    if(state.city !== '') {
      setState(state => ({...state, response: null, loading: true}))
      let url = 'https://api.openweathermap.org/data/2.5/weather?q=' + state.city + '&appid=24646f5e7d8b274514ab867de6398f31&units=metric';
      fetch(url)
        .then((response) => response.json())
        .then((responseJson) => {
          console.log(responseJson)
          setState(state => ({...state, response: responseJson, loading: false}))
        }).catch(error => {
          console.log(error)
          setState(state => ({...state, response: error,loading: false}))
        });
    } else {
      setState(state => ({...state, errorCity: 'Field ini tidak boleh kosong.'}))
    }
  }

  const renderInformasi = () => {
    return (
      <View style={{marginTop: toDp(24)}}>
        <Text style={[styles.textPukul, {color: isDarkMode ? 'white' : '#111111'}]}>Informasi Prakiraan Cuaca</Text>
        <View style={[styles.viewFormLayanan, {backgroundColor: false ? '#1C1C1E' : 'white', paddingBottom: toDp(16)}]}>
          <View style={[styles.detailRow, {paddingBottom: toDp(4)}]}>
            <Text style={[styles.textNameDetail, {color: false ? 'white' : '#111111'}]}>Code Negara</Text>
            <Text style={[styles.textDot, {color: '#111111'}]}>:</Text>
            <Text style={[styles.textPriceDetail, {color: false ? 'white' : '#111111'}]}>{state.response.sys.country}</Text>
          </View>
          <View style={[styles.detailRow, {paddingBottom: toDp(4), paddingTop: toDp(4)}]}>
            <Text style={[styles.textNameDetail, {color: false ? 'white' : '#111111'}]}>Kota/Kab</Text>
            <Text style={[styles.textDot, {color: '#111111'}]}>:</Text>
            <Text style={[styles.textPriceDetail, {color: false ? 'white' : '#111111'}]}>{state.response.name}</Text>
          </View>
          <View style={styles.line} />
          <View style={[styles.detailRow, {paddingBottom: toDp(4), paddingTop: toDp(4)}]}>
            <Text style={[styles.textNameDetail, {color: false ? 'white' : '#111111'}]}>Cuaca</Text>
            <Text style={[styles.textDot, {color: '#111111'}]}>:</Text>
            <Text style={[styles.textPriceDetail, {color: false ? 'white' : '#111111'}]}>{state.response.weather[0].main}</Text>
          </View>
          <View style={[styles.detailRow, {paddingBottom: toDp(4), paddingTop: toDp(4)}]}>
            <Text style={[styles.textNameDetail, {color: false ? 'white' : '#111111'}]}>Deskripsi</Text>
            <Text style={[styles.textDot, {color: '#111111'}]}>:</Text>
            <Text style={[styles.textPriceDetail, {color: false ? 'white' : '#111111'}]}>{state.response.weather[0].description}</Text>
          </View>
          <View style={[styles.detailRow, {paddingBottom: toDp(4), paddingTop: toDp(4)}]}>
            <Text style={[styles.textNameDetail, {color: false ? 'white' : '#111111'}]}>Temparatur</Text>
            <Text style={[styles.textDot, {color: '#111111'}]}>:</Text>
            <Text style={[styles.textPriceDetail, {color: false ? 'white' : '#111111'}]}>{state.response.main.temp + '° Celsius'}</Text>
          </View>
          <View style={[styles.detailRow, {paddingBottom: toDp(4), paddingTop: toDp(4)}]}>
            <Text style={[styles.textNameDetail, {color: false ? 'white' : '#111111'}]}>Wind Speed</Text>
            <Text style={[styles.textDot, {color: '#111111'}]}>:</Text>
            <Text style={[styles.textPriceDetail, {color: false ? 'white' : '#111111'}]}>{state.response.wind.speed}</Text>
          </View>
          <View style={[styles.detailRow, {paddingBottom: toDp(4), paddingTop: toDp(4)}]}>
            <Text style={[styles.textNameDetail, {color: false ? 'white' : '#111111'}]}>Sunrise</Text>
            <Text style={[styles.textDot, {color: '#111111'}]}>:</Text>
            <Text style={[styles.textPriceDetail, {color: false ? 'white' : '#111111'}]}>{state.response.sys.sunrise}</Text>
          </View>
          <View style={styles.line} />
          <View style={[styles.detailRow, {paddingBottom: toDp(4), paddingTop: toDp(4)}]}>
            <Text style={[styles.textNameDetail, {color: false ? 'white' : '#111111'}]}>Sunset</Text>
            <Text style={[styles.textDot, {color: '#111111'}]}>:</Text>
            <Text style={[styles.textPriceDetail, {color: false ? 'white' : '#111111'}]}>{state.response.sys.sunset}</Text>
          </View>
          <View style={[styles.detailRow, {paddingBottom: toDp(4), paddingTop: toDp(4)}]}>
            <Text style={[styles.textNameDetail, {color: false ? 'white' : '#111111'}]}>Presure</Text>
            <Text style={[styles.textDot, {color: '#111111'}]}>:</Text>
            <Text style={[styles.textPriceDetail, {color: false ? 'white' : '#111111'}]}>{state.response.main.pressure}</Text>
          </View>
          <View style={[styles.detailRow, {paddingBottom: toDp(4), paddingTop: toDp(4)}]}>
            <Text style={[styles.textNameDetail, {color: false ? 'white' : '#111111'}]}>Humidity</Text>
            <Text style={[styles.textDot, {color: '#111111'}]}>:</Text>
            <Text style={[styles.textPriceDetail, {color: false ? 'white' : '#111111'}]}>{state.response.main.humidity}</Text>
          </View>
          <View style={[styles.detailRow, {paddingBottom: toDp(4), paddingTop: toDp(4)}]}>
            <Text style={[styles.textNameDetail, {color: false ? 'white' : '#111111'}]}>Sea Level</Text>
            <Text style={[styles.textDot, {color: '#111111'}]}>:</Text>
            <Text style={[styles.textPriceDetail, {color: false ? 'white' : '#111111'}]}>{state.response.main.sea_level}</Text>
          </View>
          <View style={[styles.detailRow, {paddingBottom: toDp(4), paddingTop: toDp(4)}]}>
            <Text style={[styles.textNameDetail, {color: false ? 'white' : '#111111'}]}>Ground Level</Text>
            <Text style={[styles.textDot, {color: '#111111'}]}>:</Text>
            <Text style={[styles.textPriceDetail, {color: false ? 'white' : '#111111'}]}>{state.response.main.grnd_level}</Text>
          </View>
        </View>
      </View>
    )
  }

  return (
    <SafeAreaView style={backgroundStyle}>
      <StatusBar barStyle={isDarkMode ? 'light-content' : 'dark-content'} />
      <View style={styles.content}>
        <CustomTextInput
          isDarkMode={isDarkMode}
          error={state.errorCity}
          include={false}
          title={'Kota/Kab'}
          placeholder={'Masukan Kota/Kab'}
          value={state.city}
          onChangeText={(city) => {
            if(city === '') {
              setState(state => ({...state, city, errorCity: 'Field ini tidak boleh kosong.'}))
            } else {
              setState(state => ({...state, city, errorCity: ''}))
            }
          }}
          autoCapitalize={'none'}
          height={toDp(40)}
          maxLength={50}
        />
        <View style={styles.viewButton}>
          <CustomButton
            text={'Lihat'}
            type={'gradient'}
            onPress={() => lihat()}
          />
        </View>

        {
          (state.response == null && state.loading) ?
            <View style={styles.viewLoading}>
              <ActivityIndicator size="large" color="#EF9038" />
            </View>
          :
          state.response == null ?
            <View />
          :
          state.response.cod === '404' ?
            <View style={styles.viewLoading}>
              <Text style={{color: isDarkMode ? 'white' : '#111111'}}>{state.response.message}</Text>
            </View>
          :
          renderInformasi()
        }

      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  content: {
    height: '100%',
    paddingHorizontal: toDp(24)
  },
  viewButton: {
    marginTop: toDp(16)
  },
  textPukul: {
    fontSize: toDp(14),
    fontWeight: 'bold',
    letterSpacing: toDp(0.5),
  },
  viewFormLayanan: {
    marginTop: toDp(8),
    width: '100%',
    height: 'auto',
    borderRadius: toDp(4),
    shadowColor: "#000",
    shadowOffset: {
    	width: 0,
    	height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    elevation: 3,
  },
  viewRow: {
    marginLeft: toDp(16),
    marginTop: toDp(16),
    marginRight: toDp(16),
    flexDirection: 'row',
    alignItems: 'center'
  },
  detailRow: {
    flexDirection: 'row',
    //justifyContent: 'space-between',
    padding: toDp(16),
    alignItems: 'center',
  },
  textNameDetail: {
    fontSize: toDp(12),
    width: toDp(120),
    //backgroundColor: 'cyan'
  },
  textDot: {
    fontSize: toDp(12),
    width: toDp(8),
    fontWeight: '700'
  },
  textPriceDetail: {
    width: '56%',
    fontSize: toDp(12),
  },
  line: {
    width: '100%',
    height: toDp(1),
    backgroundColor: '#CCCCCC',
    marginVertical: toDp(8)
  },
  viewLoading: {
    flex: 1,
    justifyContent: 'center',
    alignItems:'center'
  }
});

export default App;
