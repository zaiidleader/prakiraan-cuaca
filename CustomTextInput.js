import React, { Component } from 'react'
import {BackHandler, TextInput, PixelRatio, Dimensions, StyleSheet, Text, View, Image, Alert, Platform, TouchableOpacity, AsyncStorage, ScrollView} from 'react-native'

import { toDp } from './percentageToDP'

let { width, height } = Dimensions.get('window')

class CustomTextInput extends Component {


  render() {
    const {
        title,
        error,
        maxLength,
        value,
        inputRef,
        onChangeText,
        onSubmitEditing,
        placeholder,
        secureTextEntry,
        onChangeSecure,
        type,
        generate,
        isDarkMode
    } = this.props


    return (
      <View style={styles.viewForm}>
        {
          title !== '' && <Text style={[styles.textTitle, {color: isDarkMode ? 'white' : '#111111'}]}>{title}</Text>
        }
        <View style={[styles.viewText, {backgroundColor: generate ? '#e5e5e5' : 'white'} , error !== '' && styles.viewError]}>
          <TextInput
            ref={r => inputRef && inputRef(r)}
            onChangeText={text => onChangeText(text)}
            onSubmitEditing={() => onSubmitEditing && onSubmitEditing()}
            maxLength={maxLength}
            autoCapitalize={'none'}
            underlineColorAndroid={'transparent'}
            value={value}
            secureTextEntry={secureTextEntry}
            style={[styles.textInput, {color: '#111111'}]}
            placeholder={placeholder}
            placeholderTextColor={'#8d96a6'}
            {...this.props}
          />
        </View>
        {error !== '' && <Text style={styles.textError}>{error}</Text>}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  viewForm: {
    width: '100%',
    height: 'auto',
    marginTop: toDp(16)
  },
  textTitle: {
    fontSize: toDp(14),
    letterSpacing: toDp(0.6)
  },
  viewText: {
    width: '100%',
    height: toDp(52),
    marginTop: toDp(6),
    borderRadius: toDp(6),
    backgroundColor: 'white',
    paddingHorizontal: toDp(16),
    shadowColor: "#111111",
    shadowOffset: {
    	width: 0,
    	height: 1,
    },
    shadowOpacity: 0.20,
    shadowRadius: 1.41,
    elevation: 2,
  },
  textInput: {
    flex: 1,
    fontSize: toDp(14),
    color: 'white',
    marginLeft: Platform.OS === 'android' ? toDp(-4) : 0,
    marginBottom: toDp(2)
  },
  textError: {
    marginTop: toDp(4),
    marginLeft: toDp(2),
    fontSize: toDp(14),
    color: '#F5493C',
  },
  viewError: {
    borderColor: '#F5493C',
    backgroundColor: '#ffe2dd'
  },
  customRow: {
    flexDirection: 'row',
    width: '100%',
    backgroundColor: 'red'
  },
  touchEye: {
    position: 'absolute',
    right: toDp(16),
    bottom: toDp(14)
  },
  icEye: {
    width: toDp(20.3),
    height: toDp(20),
    tintColor: '#B0BEC5',
    resizeMode: 'contain'
  }
})


export default CustomTextInput
