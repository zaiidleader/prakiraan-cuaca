# Prakiraan Cuaca

Sebagai tahap awal membuat aplikasi mobile sederhana untuk mengetahui prakiraan cuaca berbasis react-native dengan menggunakan API dari openweathermap.org.

![Screenshot aplikasi](sc.png)

Thank You<br />
Best Regards
<br /> <br />
Zaini Jam'athsani <br />
Mobile Apps Developer
<br /> <br />
Email : dleader.zaii@gmail.com <br />
Phone : +6285694084870

